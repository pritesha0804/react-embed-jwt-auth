import dash
from dash_embedded import Embeddable, EmbeddedAuth
import requests
import json
import jwt

app = dash.Dash(__name__, plugins=[Embeddable(origins=['*'])],suppress_callback_exceptions = True)

# Load public key from identity API
DISCOVERY_DOCUMENT = requests.get('https://dev.blastiq.com/identity/.well-known/openid-configuration').json()
JWKS_URI = DISCOVERY_DOCUMENT['jwks_uri']
JWKS = requests.get(JWKS_URI).json()
CLAIMS = {
    'issuer': 'https://dev.blastiq.com/identity',
    'test': 'blah'
}


class MyGlobals():
    ACCESS_TOKEN = ''  # global data

PUBLIC_KEYS = {}
for JWK in JWKS['keys']:
    kid = JWK['kid']
    PUBLIC_KEYS[kid] = jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(JWK))

# Subclass EmbeddedAuth to provide a custom `decode_token` method
class EmbeddedAuthWithCustomDecoder(EmbeddedAuth):
    def decode_token(self, token, app_url):
        try:
            kid = jwt.get_unverified_header(token)['kid']
            key = PUBLIC_KEYS[kid]
            MyGlobals.ACCESS_TOKEN = token
            return jwt.decode(
                token.strip(),
                key,
                algorithms=['RS256'],
                audience='cosmos.api',
                **CLAIMS
            )
        except (jwt.InvalidTokenError, jwt.InvalidAudienceError):
            traceback.print_exc()
            flask.abort(403)

auth = EmbeddedAuthWithCustomDecoder([app], None, None)