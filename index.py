import dash
from dash.dependencies import Input, Output
import dash_design_kit as ddk
import dash_core_components as dcc
import dash_html_components as html
import dash_table

import json
import os
import requests
import sys

import utils as utils
from app import app, auth, MyGlobals
import pages
server = app.server


# to view standalone Dash app locally, set utils.py/HOST_APP_PATHNAME = '/'
# or use env variable IGNORE_HOST_APP_PATHNAME = 'true'
if 'IGNORE_HOST_APP_PATHNAME' in os.environ and os.environ['IGNORE_HOST_APP_PATHNAME'].upper() == 'TRUE':
    strip_relative_path = app.strip_relative_path
    get_relative_path = app.get_relative_path
else:
    strip_relative_path = utils.strip_host_relative_path
    get_relative_path = utils.get_host_relative_path

app.layout = ddk.App([
    ddk.Header([
        ddk.Logo(src=utils.embedded_asset_url('logo.png')),
        ddk.Title('Analytics'),
        ddk.Menu([
            dcc.Link(
                href=get_relative_path('/'),
                children='Home'
            ),
            dcc.Link(
                href=get_relative_path('/historical-view'),
                children='Historical View'
            ),
            dcc.Link(
                href=get_relative_path('/predicted-view'),
                children='Predicted View'
            ),
        ])
    ]),

    dcc.Location(id='url'),
    html.Div(id='content')
])


@app.callback(Output('content', 'children'), [Input('url', 'pathname')])
def display_content(pathname):
    # Retreive secure data from host app. In this example we can retrieve the
    # entire dictionary including username, employee name, and company name,
    # which can be used in your callback function!
    user_data = auth.get_user_data(app)
    print(pathname)
    siteId = pathname.split('/')[2]
    page_name = strip_relative_path(pathname)
    headers = {'Authorization': 'Bearer ' + MyGlobals.ACCESS_TOKEN}
    try:
        r = requests.get('https://dev.blastiq.com/security/api/v1/userAccess/checkSiteAccess?siteId=' + siteId + '&requiredSiteRole=read', headers=headers)
        r.raise_for_status()
    except requests.exceptions.RequestException as e:
        return html.Div([
            html.Pre('Unauthorized to see the dashboard for this site')
        ])
        

    if not page_name:  # None or ''
        return html.Div([
            html.Pre(json.dumps(user_data, indent=2), style={'overflow': 'scroll'}),
            pages.home.layout
        ])
    elif page_name == 'historical-view':
        return pages.historical_view.layout
    elif page_name == 'predicted-view':
        return pages.predicted_view.layout


if __name__ == '__main__':
    app.run_server(debug=False)
